function makeLocalBackup(){
    echo ">>>> making local backup / tworzenie lokalnego zrzutu danych"

    me=$1
    date=$2

    core_backup_path='/var/lib/docker/backups/backup_'$(date "+%FT%T")
    echo ">>>> backup path / lokalizacja backupu: "$core_backup_path
    volumes_backup_path=$core_backup_path/volumes/
    mkdir -p $volumes_backup_path
    cp -R /var/lib/docker/volumes/rcp_* $volumes_backup_path
    cp -R /var/lib/docker/volumes/metadata.db $volumes_backup_path 

    migrations_backup_path=$core_backup_path/migrations/
    mkdir -p $migrations_backup_path

    SERVICES=( "Permission" "Reports" "Schedule" )

    for i in "${SERVICES[@]}"
    do
        mkdir -p $migrations_backup_path/Backend-$i/
        cp -R /home/$me/rcp/Backend-${i}/${i}Proj/${i}API/migrations/ $migrations_backup_path/Backend-$i/
    done
}

function updateVolumes(){
    echo '>>>> update volumes / przerzucenie volumenów'

    me=$1
    date=$2
    mv /var/lib/docker/volumes/rcp_permission_static_files_volume /var/lib/docker/volumes/_rcp_permission_static_files_volume
    rm -rf /var/lib/docker/volumes/rcp_*
    mv /var/lib/docker/volumes/_rcp_permission_static_files_volume /var/lib/docker/volumes/rcp_permission_static_files_volume
    cp -R /home/$me/zrzutki/$date/dbs/* /var/lib/docker/volumes/
    cp -R /home/$me/zrzutki/$date/dbs/metadata.db /var/lib/docker/volumes/

    # echo "ls /var/lib/docker/volumes/"
    # echo $(ls /var/lib/docker/volumes/)
    # echo ""
}

function updateMigrations(){
    echo '>>>> updateMigrations / przerzucenie plików z migracjami'

    me=$1
    date=$2

    SERVICES=( "Permission" "Reports" "Schedule" )

    for i in "${SERVICES[@]}"
    do
        rm -rf /home/$me/rcp/Backend-${i}/${i}Proj/${i}API/migrations/*
        cp -R /home/$me/zrzutki/$date/migrations/Backend-$i/migrations/* /home/$me/rcp/Backend-${i}/${i}Proj/${i}API/migrations/
        # echo /home/$me/rcp/Backend-${i}/${i}Proj/${i}API/migrations/
        # echo $(ls /home/$me/rcp/Backend-${i}/${i}Proj/${i}API/migrations/)
        # echo ""
    done
}

if [ $# -lt 1 ]; then 
    echo "========================================"
    echo "$0 <username> [date in YYYY-mm-dd format]"
    echo "========================================"
    echo "#        Example: (Przykład:)          #"
    echo "========================================"
    echo ""
    echo "sudo su"
    echo "$0 devadmin 2021-09-20";
    echo ""
    echo "========================================"
    echo ""
    exit 1
fi

if [ -z $2 ]; then 
    echo "\$2 is missing"
    echo "\$2 set to default value: "$(date '+%F')
    set -- "${@:1}" $(date '+%F')
    echo "\$2 value is: '$2'"
fi

makeLocalBackup $1 $2
updateVolumes $1 $2
updateMigrations $1 $2

