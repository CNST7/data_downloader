#!/bin/bash

function copyMigrations() {
    echo '>>>>  making dump on external server / tworzenie zrzutu na serverze zewnętrznym'

    SERVICES=( "Permission" "Reports" "Schedule" )
    DBS=( "permission" "reports" "schedule" )

    date=$(date '+%Y-%m-%d')

    USER_NAME=$1
    USER_NAME="${USER_NAME%%@*}"

    echo "User name: "
    echo "$USER_NAME"

    if [[ -d "/home/$USER_NAME/zrzutki/$date" ]]
    then
        echo "/home/$USER_NAME/zrzutki/$date exists on your filesystem."
        rm -rf /home/$USER_NAME/zrzutki/$date
        echo ">>>> prev dump cleaned up / wczyszczono poprzedni zrzut"
    fi

    mkdir -p /home/$USER_NAME/zrzutki/$date/dbs
    mkdir -p /home/$USER_NAME/zrzutki/$date/migrations

    echo ">>>> Prepare migrations dump / Przygotowanie zrzutu migracji"
    for i in "${SERVICES[@]}"
    do
        mkdir -p /home/$USER_NAME/zrzutki/$date/migrations/Backend-$i
        cp -r /home/$USER_NAME/rcp/Backend-${i}/${i}Proj/${i}API/migrations/ /home/$USER_NAME/zrzutki/$date/migrations/Backend-$i/
        sudo rm -rf /home/$USER_NAME/rcp/Backend-${i}/${i}Proj/${i}API/migrations/__pycache__
    done

    echo ">>>> Prepare volumes dump / Przygotowanie zrzutu volumenów"
    for i in "${DBS[@]}"
    do
        sudo -S cp -r /var/lib/docker/volumes/rcp_${i}_db_volume /home/$USER_NAME/zrzutki/$date/dbs/rcp_${i}_db_volume
    done

    sudo cp /var/lib/docker/volumes/metadata.db /home/$USER_NAME/zrzutki/$date/dbs/
    sudo chown -R $USER_NAME:$USER_NAME /home/$USER_NAME/zrzutki/$date/
    
}

function copyMigrationsToLocal(){
    echo '>>>> copy data dump from external server / skopiowanie zrzutu z zewnątrznego serwera'

    USER_NAME=$1
    USER_NAME="${USER_NAME%%@*}"

    me=$(whoami)

    date=$(date '+%Y-%m-%d')
    scp -r -P $2 -i $3 ${1}:/home/$USER_NAME/zrzutki/$date /home/$me/zrzutki/$date
}

if [ $# -lt 1 ]; then
    echo "przyklad wywolania: (example:) "
    echo "$0 <user_name@ip_address> <port>"
    exit 1
fi

ssh -t $1 -p $2 -i $3 "$(typeset -f copyMigrations $1); copyMigrations $1"
copyMigrationsToLocal $1 $2 $3
